#!/usr/bin/env bash

./post-git.sh
aur sync --repo MelonBread --root "$PWD/MelonBread/x86_64" --noview -Rfu $@ 
./pre-git.sh

echo -e "\n\n\e[1;32;4;40mNow ready to commit changes!\e[0m"
