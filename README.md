# arch-repo

Some packages I like/use on the AUR.
More or less for my personal use though anyone is welcome to use it.
Still new at this so expect issues from time to time.


To use add the following to your `/etc/pacman.conf`:

```
[MelonBread]                                                                                                                                                
SigLevel = Optional TrustAll                                                                                                                                                  
Server = https://gitlab.com/Melon-Bread/arch-repo/-/raw/master/$repo/$arch/
```

-----

## TODO:

- ~~Script automating updates for me~~
- ~~Compress packages to save space~~
- Stop using git hosting
- Handle `-git` packages better
- Figure out how to properly sign my packages
